package main

import (
	"github.com/PuerkitoBio/goquery"
	"github.com/labstack/echo"
	"net/http"
)

func main() {
	e := echo.New()
	e.GET("/ip", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]string{"origin": c.RealIP()})
	})
	e.GET("/test", func(c echo.Context) error {
		doc, err := goquery.NewDocument("URLを入れる")
		if err != nil {
			panic(err)
		}
		return c.JSON(http.StatusOK, map[string]string{"test": doc.Find("#main").Find(".root_list").Find(".show_popup").Find(".time").Text()})
	})
	e.Logger.Fatal(e.Start(":1323"))
}
